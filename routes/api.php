<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentApiController;
use App\Http\Controllers\AuthController;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

// });

Route::middleware('auth:sanctum')->group(function () {

    Route::get('/show', [StudentApiController::class, 'show']);
    Route::post('/create', [StudentApiController::class, 'create']);
    Route::get('/delete/{id}', [StudentApiController::class, 'delete']);
    Route::post('/update', [StudentApiController::class, 'update']);
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/signup', [AuthController::class, 'signup']);
// Route::post('/logout', [AuthController::class, 'logout']);
Route::post('/logout', [AuthController::class, 'logout']);
// Route::post('/logout', 'AuthController@logout')->middleware('auth:sanctum');
