<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

Route::get('/', function () {
    return view('login');
});
Route::get('/signup', function () {
    return view('/signup');
});
// Route::post('/logout', 'AuthController@logout')->name('logout')->middleware('auth');
Route::middleware('AuthCheck')->group(function(){
    Route::get('/form',[StudentController::class,'add']);
    Route::get('/table',[StudentController::class,'showdata']);
    Route::get('/edit/{id}',[StudentController::class,'update']);
});


