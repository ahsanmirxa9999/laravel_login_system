-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2024 at 04:49 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_login_sys`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2024_01_15_110838_create_student_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2024-01-16T15:23:03.000000Z\",\"created_at\":\"2024-01-16T15:23:03.000000Z\",\"updated_at\":\"2024-01-16T15:23:03.000000Z\"}', 'e23a9eca9122d3cf339a2ae4fb9c73e87111267a6aa51219e2f82482d52011fe', '[\"*\"]', NULL, NULL, '2024-01-16 12:43:13', '2024-01-16 12:43:13'),
(2, 'App\\Models\\User', 1, '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2024-01-16T15:23:03.000000Z\",\"created_at\":\"2024-01-16T15:23:03.000000Z\",\"updated_at\":\"2024-01-16T15:23:03.000000Z\"}', '74f991f48f3dce5bdf4c4362f7753df3ce109854b0d59634e2173d807f11b77c', '[\"*\"]', NULL, NULL, '2024-01-16 12:43:51', '2024-01-16 12:43:51'),
(3, 'App\\Models\\User', 1, '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2024-01-16T15:23:03.000000Z\",\"created_at\":\"2024-01-16T15:23:03.000000Z\",\"updated_at\":\"2024-01-16T15:23:03.000000Z\"}', '19f99ee154a40609fa5339fec2eebb485af97239de277da1015dcb849e25392c', '[\"*\"]', NULL, NULL, '2024-01-16 12:47:38', '2024-01-16 12:47:38'),
(4, 'App\\Models\\User', 1, '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2024-01-16T15:23:03.000000Z\",\"created_at\":\"2024-01-16T15:23:03.000000Z\",\"updated_at\":\"2024-01-16T15:23:03.000000Z\"}', '59ccc1a7ab36962fc253bb7a1687f3961a250e6809f925e4f0d076f9aa46acdb', '[\"*\"]', NULL, NULL, '2024-01-16 12:52:52', '2024-01-16 12:52:52'),
(5, 'App\\Models\\User', 1, '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2024-01-16T15:23:03.000000Z\",\"created_at\":\"2024-01-16T15:23:03.000000Z\",\"updated_at\":\"2024-01-16T15:23:03.000000Z\"}', '35313ed860dcf97e9b4e85802a57680e78bc5c8e06ba649b09939cf140ede1f6', '[\"*\"]', NULL, NULL, '2024-01-16 13:16:52', '2024-01-16 13:16:52'),
(6, 'App\\Models\\User', 1, '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2024-01-16T15:23:03.000000Z\",\"created_at\":\"2024-01-16T15:23:03.000000Z\",\"updated_at\":\"2024-01-16T15:23:03.000000Z\"}', '755a3787b6de31b65dd4e578cf46aa2f78e67d0ebd65710b3ee0486924296471', '[\"*\"]', NULL, NULL, '2024-01-16 13:16:58', '2024-01-16 13:16:58'),
(7, 'App\\Models\\User', 1, '{\"id\":1,\"name\":\"admin\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2024-01-16T15:23:03.000000Z\",\"created_at\":\"2024-01-16T15:23:03.000000Z\",\"updated_at\":\"2024-01-16T15:23:03.000000Z\"}', '71d57a596a7f19ef6e4caef9310b2a3ab5e663115c25959d9a4dd03b3c00b62d', '[\"*\"]', NULL, NULL, '2024-01-16 13:17:28', '2024-01-16 13:17:28'),
(8, 'App\\Models\\User', 2, 'MyApp', 'b51e8e139502051a715f7872081b9ebb84dc7031a7821f4d1f8e699ad4092131', '[\"*\"]', NULL, NULL, '2024-01-17 04:48:06', '2024-01-17 04:48:06'),
(9, 'App\\Models\\User', 3, 'MyApp', '74c5d570add05d181bba563ed9c8079ffc1a61a41dc218e246e8069d90ff32fc', '[\"*\"]', NULL, NULL, '2024-01-17 04:50:05', '2024-01-17 04:50:05');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `des` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `lname`, `email`, `address`, `dob`, `phone`, `des`, `created_at`, `updated_at`) VALUES
(8, 'Hawkins', 'Dean', 'nukotot@mailinator.com', 'Powell', '2011-05-28', '787', 'Ut pariatur Esse in', '2024-01-15 08:28:31', '2024-01-15 08:28:31'),
(9, 'Joyce', 'Le', 'dogucom@mailinator.com', 'Marquez', '2014-09-23', '700', 'Temporibus qui dolor', '2024-01-15 09:10:24', '2024-01-15 09:10:24'),
(10, 'Dejesus', 'Pratt', 'fakuhiw@mailinator.com', 'Mathews', '1972-06-29', '112', 'Minima incididunt fu', '2024-01-16 12:58:18', '2024-01-16 12:58:18'),
(11, 'French', 'Curry', 'hyrazyfuqu@mailinator.com', 'Briggs', '1981-10-06', '646', 'Beatae in minim rati', '2024-01-17 08:02:30', '2024-01-17 08:02:30'),
(12, 'Green', 'Mcdaniel', 'vucu@mailinator.com', 'Rodgers', '2015-01-23', '644', 'Rerum dolor aut et s', '2024-01-17 08:43:56', '2024-01-17 08:43:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '2024-01-16 10:23:03', '$2y$12$c9ybcI.lRaNAAcZD9jbx1O.ylkfhXK6lQ2jNOTMlAvWW7g8v65N4u', 'tEWzVPl51g', '2024-01-16 10:23:03', '2024-01-16 10:23:03'),
(2, 'Abraham Holden', 'qocawikugi@mailinator.com', NULL, '$2y$12$qLyeKZs.aek6E/1zc2OXmOt9Xxt8FtKwHsIrh282XTrvaeH4evUTC', NULL, '2024-01-17 04:48:05', '2024-01-17 04:48:05'),
(4, 'User', 'Umer@email.com', NULL, '$2y$12$07LMBdTkf4V7pfVD2zMsh.GWcKEqc1Kwti8u9MrcSLP51rxGP6JUq', NULL, '2024-01-17 04:52:03', '2024-01-17 04:52:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
