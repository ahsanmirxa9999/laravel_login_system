<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class StudentApiController extends Controller
{

    public function create(Request $request)
    {
        // dd($request->all());
        $create = Student::create([
            'name' => $request->name,
            'lname' => $request->lname,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'phone' => $request->phone,
            'des' => $request->des,
        ]);
        if ($create) {
            return response()->json([
                'status' => 200,
                'done' => 'Student has been Inserted Successfully'
            ]);
        }
    }

    public function show()
    {
        $data = Student::all();
        // dd($data);
        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function delete($id)
    {

        $delete = Student::find($id)->delete();
        if ($delete) {
            return response()->json([
                'status' => 200,
                'delete' => 'Student Has Been Deleted'
            ]);
        }
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $update = Student::where('id', $request->sid)->update([
            'name' => $request->name,
            'lname' => $request->lname,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'phone' => $request->phone,
            'des' => $request->des,
        ]);
        if ($update) {
            return response()->json([
                'status' => 200,
                'done' => 'Student has been Inserted Successfully'
            ]);
        }
    }

}
