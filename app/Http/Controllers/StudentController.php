<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;

class StudentController extends Controller
{
    public function add(){
        return view('form');
    }

    public function showdata(){
        return view('/table');
    }

    public function update($id){
        $update = Student::find($id);
        return view('update')->with(compact('update'));
    }

}
