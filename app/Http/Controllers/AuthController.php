<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
// use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt(['email' => $request->name, 'password' => $request->password])) {
            $user = Auth::user();
            $token = $user->createToken($user)->plainTextToken;
            return response()->json([
                'token' => $token,
                'status' => 200,
                'login' => 'User Successfully Login',
            ]);
        } else {
            return response()->json([
                'invalid' => 'User is Invalid',
            ]);
        }
    }

    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();
        $token = $user->createToken($user)->plainTextToken;
        $success = [
            'name' => $user->name,
            'email' => $user->email,
            'token' => $token,
        ];
        $response = [
            'success' => true,
            'data' => $success,
            'message' => 'User registered successfully',
        ];
        return response()->json($response, 200);
    }

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json(['message' => 'Logout successful'], 200);
    }
}
