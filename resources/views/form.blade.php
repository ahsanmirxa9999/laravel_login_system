<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" type="text/css"href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1 class="text-primary mt-4">Student Form</h1>
            </div>
            {{-- <div class="col-md-1 mt-4">
                @auth
                <form action="{{ url('logout') }}" method="POST" id="logout">
                    @csrf
                    <button type="submit" class="btn btn-primary" id="btn">Logout</button>
                </form>
            @endauth
            </div> --}}
            <div class="col-md-1 mt-4">
                <a class="btn btn-primary" href="{{ url('/') }}" role="button">Logout</a>
            </div>
            <div class="col-md-2 mt-4">
                <a class="btn btn-primary" href="{{ url('table') }}" role="button">View Table</a>
            </div>
        </div>
        <form action="" method="POST" id="myform">
            @csrf
            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" name="name" id=""
                            aria-describedby="helpId" placeholder="Enter Your Name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Last Name</label>
                        <input type="text" class="form-control" name="lname" id=""
                            aria-describedby="helpId" placeholder="Enter Your Last Name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" class="form-control" name="email" id=""
                            aria-describedby="helpId" placeholder="Enter Your Email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Address</label>
                        <input type="text" class="form-control" name="address" id=""
                            aria-describedby="helpId" placeholder="Enter Your Address">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Date Of Birth</label>
                        <input type="date" class="form-control" name="dob" id=""
                            aria-describedby="helpId" placeholder="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Phone Number</label>
                        <input type="number" class="form-control" name="phone" id=""
                            aria-describedby="helpId" placeholder="Enter Your Number">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="des" id="" rows="3">Type Message...</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-2">
                    <button class="btn btn-primary" id="btn" role="button">Submit</button>
                </div>
            </div>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


</body>

</html>

<script>
    $('#btn').on('click', function(e) {
        e.preventDefault();
        let data = new FormData(myform)
        $.ajax({
            url: "http://127.0.0.1:8000/api/create",
            method: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.done) {
                    toastr.success(res.done, 'Data!', {
                        timeOut: 2000
                    })
                }
                $('#myform').trigger('reset');
            }
        });
    });
</script>

<script>
    toastr.options = {
        "closeButton": true,
        "progressBar": true
    }
</script>

{{-- <script>
$('#btn').on('click', function(e) {
    e.preventDefault();
    var data = $('#logout').serialize();
    $.ajax({
        url: "http://127.0.0.1:8000/api/logout",
        method: 'POST',
        data: data,
        success: function(res) {
            if (res.message === 'Logout successful') {
                window.location.href = "http://127.0.0.1:8000/";
            }
        },
        error: function(xhr, status, error) {
            console.error(xhr.responseText);
        }
    });
});
</script> --}}
