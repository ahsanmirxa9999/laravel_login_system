<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student Management System</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- Font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css"
        rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />


</head>

<body>
    <div class="container" style="margin-top: 50px;">
        <h3 class="text-center text-primary"><b>Student Management System</b> </h3>
        <a href="{{ url('/') }}" class="btn btn-primary mb-2">Add Student</a>

        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Birthday</th>
                    <th>Phone Number</th>
                    <th>Description</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody id="bodyData">

            </tbody>
        </table>
    </div>
</body>

</html>
{{-- <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script> --}}
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
{{-- <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script> --}}


<script>
    // ============== Delete Toastr ==================
    toastr.options = {
        "closeButton": true,
        "progressBar": true
    }
</script>

<script>
    function show(params) {
        $.ajax({
            url: "http://127.0.0.1:8000/api/show",
            method: "GET",
            success: function(res) {
                // console.log(res);
                let row = "";
                res.data.forEach((element, index) => {
                    console.log(element)
                    row = row + `
                    <tr>
                        <td>${index+1}</td>
                        <td>${element.name}</td>
                        <td>${element.lname}</td>
                        <td>${element.email}</td>
                        <td>${element.address}</td>
                        <td>${element.birthday}</td>
                        <td>${element.phone}</td>
                        <td>${element.des}</td>
                        <td><a href="{{ url('/edit/${element.id}') }}"><button class='btn btn-success'>Update</button></a></td>
                        <td><button class='btn btn-danger' onclick="deletefun(${element.id})">Delete</button></td>
                    </tr>
                    `
                });
                document.getElementById('bodyData').innerHTML = row
            }
        });
    }
    show();

    function deletefun(id) {
        $.ajax({
            url: "http://127.0.0.1:8000/api/delete/" + id,
            method: "GET",
            success: function(res) {
                if (res.delete) {
                    show();
                    toastr.error(res.delete, 'Data!', {
                        timeOut: 2000
                    });
                } else {
                    alert('Not deleted');
                }
            }
        });
    }
</script>
